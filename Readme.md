# Einstein's riddle

This repo contains simple solution in Prolog for Einstein's riddle.

## Einstein's riddle - Polish version

Pięć osób o różnych narodowości zamieszkuje 5 domów w 5 różnych kolorach.
Każda z osób pali 5 różnych wyrobów tytoniowych i pije 5 różnych napojów. Hodują oni zwierzęta 5 różnych gatunków. Który z nich trzyma w domu rybki? Który z nich pije wodę?

1. Kanadyjczyk zamieszkuje pierwszy dom
2. Hiszpan mieszka w czerwonym domu.
3. Zielony dom znajduje się bezpośrednio po lewej stronie domu białego.
4. Szkot pija herbatkę.
5. Palacz papierosów light mieszka obok hodowcy kotów.
6. Mieszkaniec żółtego domu pali cygara.
7. Niemiec pali fajkę.
8. Mieszkaniec środkowego domu pija mleko.
9. Palacz papierosów light ma sąsiada, który pija wodę.
10. Palacz papierosów bez ﬁltra hoduje ptaki.
11. Francuz hoduje psy.
12. Kanadyjczyk mieszka obok niebieskiego domu.

## Einstein's riddle - English version

Five people with a different nationalities live in 5 houses in different colors.
Each person smokes a certain brand of cigar and drinks a certain type of drink. All of them breeds a certain pet. No owners have the same pet, smoke the same brand of cigar or drink the same beverage. Who breeds fish? Who drinks water?

1. Canadian lives in a first house
2. Spaniard lives in a red house
3. Green house is on the left from of the white house
4. Scot drinks tea
5. Person, who smoke lights, lives next to the cat breeder    
6. Person, who lives in a yellow house, smokes cigar
7. German smokes pipe
8. Person, who lives in a middle, drinks milk
9. Neighbour of person, who smokes lights, drinks water
10. Person, who smokes cigarettes without a filter, breeds birds
11. French breeds dogs
12. Canadian lives next to the blue house

## Requirements

You have to install SWI Prolog interpreter before run this code. It can be downloaded from [this site](https://www.swi-prolog.org/download/stable).

## How to run

Install SWI Prolog interpreter: 

    sudo apt-get install swi-prolog

Go to appropiate directory:

    cd DIRECTORY_WITH_CODE

Run SWI Prolog interpreter:

    swipl -s einstein.pro

Compile program:

    make

Type chosen command. This code could be run in 3 different ways:  
Display solution for two questions: 

    get_solution(BreedsFish, DrinksWater).

Display the person who breeds fish:

    who_breeds_fish(Answer).

Display the person who drinks water:

    who_drinks_water(Answer).

