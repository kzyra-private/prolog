/*
EINSTEIN's RIDDLE - PROLOG
*/

get_solution(BreedsFish, DrinksWater) :-
    who_breeds_fish(BreedsFish),
    who_drinks_water(DrinksWater).

who_breeds_fish(Answer) :-
    neighborhood(Nbh),
    member(house(Answer,_,_,_,fish), Nbh).

who_drinks_water(Answer) :-
    neighborhood(Nbh),
    member(house(Answer,_,water,_,_), Nbh).

neighborhood(Nbh) :-
    length(Nbh, 5),
    set_first(house(canadian,_,_,_,_), Nbh),
    member(house(spanish,red,_,_,_), Nbh),
    on_left(house(_,green,_,_,_), house(_,white,_,_,_), Nbh),
    member(house(scottish,_,tea,_,_), Nbh),
    are_neighbors(house(_,_,_,light,_), house(_,_,_,_,cats), Nbh),
    member(house(german,_,_,pipe,_), Nbh),
    set_middle(house(_,_,milk,_,_), Nbh),
    are_neighbors(house(_,_,_,light,_), house(_,_,water,_,_), Nbh),
    member(house(_,_,_,unfiltered, birds), Nbh),
    member(house(french,_,_,_,dogs), Nbh),
    are_neighbors(house(canadian,_,_,_,_), house(_,blue,_,_,_), Nbh),
    are_neighbors(house(_,_,_,_,horses), house(_,yellow,_,_,_), Nbh),
    member(house(_,_,beer,menthols,_), Nbh),
    member(house(_,green,_,_,_), Nbh),
    member(house(_,_,_,_,fish), Nbh).

are_neighbors(House1, House2, Houses) :-
    nextto(House1, House2, Houses);
    nextto(House2, House1, Houses).

on_left(House1, House2, Houses) :-
    nextto(House1, House2, Houses).

set_middle(House, Houses) :-
    nth0(2, Houses, House).

set_first(House, Houses) :-
    nth0(0, Houses, House).
    